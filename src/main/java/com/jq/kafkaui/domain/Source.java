package com.jq.kafkaui.domain;

import lombok.Data;

@Data
public class Source {
    Integer id;
    String name;
    String broker;
}
